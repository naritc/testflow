package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public static void test() {
		StringBuilder sb = new StringBuilder();
		sb.append("1");
		sb.append("2");
		sb.append("3");
		sb.append("4");
		sb.append("5");

		System.out.println(sb.toString());
	}

	public static void addFeature1() {
		System.out.println("Feature 1 added");
	}

}
